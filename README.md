# Declarative Ham Radio



## Getting started

Nothing much to see here yet. Tinkering around with different approaches to making ham radio software easier to install and maintain.
Consider everything in here alpha, but maybe something will come of it.

## Usage examples
### Display JSON (parsed from INI) of entire INI file
```wsjtx.py -d "%userprofile%\AppData\Local\WSJT-X - InstanceName"```

### Display JSON (parsed from INI), filtered to callsign-related settings
```wsjtx.py -d -f callsign "%userprofile%\AppData\Local\WSJT-X - InstanceName"```

### Display JSON (parsed from INI), filtered to frequent settings
```wsjtx.py -d -f frequent "%userprofile%\AppData\Local\WSJT-X - InstanceName"```

### Update INI file with changes provided in JSON declaration file
This declaration only needs to include the keys you wish to update. Use the Display functionality above to understand the schema.

```wsjtx.py -j updates.json  "%userprofile%\AppData\Local\WSJT-X - InstanceName"```
