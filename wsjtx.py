import argparse
import configparser
import json
import os

# Define sets of filters the user is probably interested in changing.
# This is used when -f (filter) is provided. .
FILTER_SETTINGS = {
    'frequent': {
        'Configuration': ['MyCall', 'OpCall', 'MyGrid','SaveDir','AzElDir','SoundInName','SoundOutName',
                          'Rig', 'CATNetworkPort', 'CATUSBPort', 'CATSerialPort','CATSerialRate',
                          'PTTMethod', 'PTTport','Mode', 'ModeTx', 'wd_FT4', 'wd_FT8', 'wd_Timer'
                          'qrzComUn','qrzComPw','PSKReporter',
                          'UDPServer','UDPServerPort','AcceptUDPRequests','udpWindowToFront'],
        'Common': ['Mode', 'ModeTx'],
        'MainWindow': ['MRUdir']
    },
    'callsign': {
        # Callsign-specific settings
        'Configuration': ['MyCall', 'OpCall','SaveDir','AzElDir'],
        'MainWindow': ['MRUdir']
    }
    # Add more sets down the road?
}

def readIniToJson(fileName, filter_name=None):
    """
    Convert the content of an INI file to a JSON string based on a filter.

    :param fileName: Path to the INI file.
    :param filter_name: Name of the filter to apply. None or empty string for no filter.
    :return: Pretty-printed JSON representation of the INI file content.
    """
    config = configparser.ConfigParser(interpolation=None)
    config.optionxform=str
    config.read(fileName)
    
    if filter_name and filter_name in FILTER_SETTINGS:  # Check if filter name is provided and exists in our dictionary
        filterData = {}
        for section, keys in FILTER_SETTINGS[filter_name].items():
            if section in config:
                filterData[section] = {k: config[section][k] for k in keys if k in config[section]}
        return json.dumps(filterData, indent=4)

    return json.dumps({s: dict(config.items(s)) for s in config.sections()}, indent=4)


def updateIniFromJsonFile(fileName, jsonFile):
    """
    Update the content of an INI file using a JSON file.

    :param fileName: Path to the INI file.
    :param jsonFile: Path to the JSON file containing updates.
    """
    config = configparser.ConfigParser(interpolation=None)
    config.optionxform=str
    config.read(fileName)
    
    with open(jsonFile, 'r') as file:
        dataToUpdate = json.load(file)
        
    for section, options in dataToUpdate.items():
        if section not in config:
            config[section] = {}
        for option, value in options.items():
            config[section][option] = value
    
    with open(fileName, 'w') as configFile:
        config.write(configFile)

def getIniFilePath(path):
    """
    Generate the path to an INI file. If a directory is provided, the function 
    assumes the INI file name matches the directory name.

    :param path: Path to either a directory or an INI file.
    :return: Resolved path to the INI file.
    """
    if os.path.isdir(path):
        folderName = os.path.basename(path)
        return os.path.join(path, folderName + ".ini")
    else:
        return path

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Manipulate WSJT-X config files.")
    parser.add_argument(
        "paths", 
        help="Comma-separated list of paths to WSJT-X INI config files or directories. " + 
             "Example path: \"%%userprofile%%\\AppData\\Local\\WSJT-X - InstanceName\\\"",
        metavar="INI_PATHS"
    )
    parser.add_argument(
        "-d", "--display", 
        action="store_true", 
        help="Display the INI file as JSON."
    )
    parser.add_argument(
        "-f", "--filter", 
        default="",  
        choices=FILTER_SETTINGS.keys(),
        help="Filter settings displayed in the INI file as JSON. Used in conjunction with --display."
    )
    parser.add_argument(
        "-j", "--json2ini",
        help="Update the INI files from a JSON file.",
        metavar="<json file>"
    )
    parser.add_argument(
        '-?', action='help', 
        default=argparse.SUPPRESS,
        help='Show this help message and exit.'
    )

    args = parser.parse_args()
        
    paths = args.paths.split(',')
    iniFiles = [getIniFilePath(path) for path in paths]

    if args.display:
        for iniFile in iniFiles:
            print(f"# Content of {iniFile}:")
            print(readIniToJson(iniFile, args.filter))
            print("\n" + "-" * 40 + "\n")
    
    if args.json2ini:
        for iniFile in iniFiles:
            updateIniFromJsonFile(iniFile, args.json2ini)
            print(f"Updated settings in {iniFile} from provided JSON file.")
